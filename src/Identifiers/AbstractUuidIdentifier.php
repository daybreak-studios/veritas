<?php
	namespace DaybreakStudios\Veritas\Identifiers;

	use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

	abstract class AbstractUuidIdentifier implements IdentifierInterface {
		/**
		 * @var callable
		 */
		private $generator;

		/**
		 * UuidIdentifier constructor.
		 *
		 * @param callable $generator
		 */
		public function __construct(callable $generator) {
			$this->generator = $generator;
		}

		/**
		 * {@inheritdoc}
		 */
		public function generate() {
			try {
				return call_user_func($this->generator);
			} catch (UnsatisfiedDependencyException $e) {
				throw new \RuntimeException('Could not generate UUID: ' . $e->getMessage());
			}
		}
	}