<?php
	namespace DaybreakStudios\Veritas;

	use DaybreakStudios\Veritas\Exception\TokenExpiredException;
	use DaybreakStudios\Veritas\Exception\TokenInvalidException;
	use DaybreakStudios\Veritas\Exception\TokenNotYetValidException;
	use DaybreakStudios\Veritas\Identifiers\IdentifierInterface;
	use DaybreakStudios\Veritas\Signers\SignerInterface;
	use DaybreakStudios\Veritas\Token\Token;
	use DaybreakStudios\Veritas\Token\TokenHeaderSet;
	use DaybreakStudios\Veritas\Token\TokenInterface;
	use DaybreakStudios\Veritas\TokenBuilder\TokenBuilder;
	use DaybreakStudios\Veritas\TokenBuilder\TokenBuilderInterface;

	class Veritas {
		/**
		 * @var SignerInterface
		 */
		private $signer;

		/**
		 * @var string
		 */
		private $secret;

		/**
		 * @var int
		 */
		private $ttl;

		/**
		 * @var \ReflectionClass
		 */
		private $tokenBuilderClass;

		/**
		 * @var IdentifierInterface|null
		 */
		private $identifier = null;

		/**
		 * Veritas constructor.
		 *
		 * @param SignerInterface $signer
		 * @param string          $secret
		 * @param int             $ttl
		 * @param string|null     $tokenBuilderClass
		 *
		 * @throws \ReflectionException
		 */
		public function __construct(SignerInterface $signer, $secret, $ttl, $tokenBuilderClass = null) {
			$this->signer = $signer;
			$this->secret = $secret;
			$this->ttl = $ttl;
			$this->tokenBuilderClass = new \ReflectionClass($tokenBuilderClass ?: TokenBuilder::class);
		}

		/**
		 * @return SignerInterface
		 */
		public function getSigner() {
			return $this->signer;
		}

		/**
		 * @return string
		 */
		public function getSecret() {
			return $this->secret;
		}

		/**
		 * @return int
		 */
		public function getTokenLifetime() {
			return $this->ttl;
		}

		/**
		 * @return IdentifierInterface|null
		 */
		public function getIdentifier() {
			return $this->identifier;
		}

		/**
		 * @param IdentifierInterface|null $identifier
		 *
		 * @return $this
		 */
		public function setIdentifier(IdentifierInterface $identifier = null) {
			$this->identifier = $identifier;

			return $this;
		}

		/**
		 * @return TokenBuilderInterface
		 */
		public function build() {
			/** @noinspection PhpIncompatibleReturnTypeInspection */
			return $this->tokenBuilderClass->newInstance($this);
		}

		/**
		 * @param array           $data
		 * @param int|string|null $subject
		 * @param int|null        $ttl
		 *
		 * @return string
		 */
		public function issue(array $data = [], $subject = null, $ttl = null) {
			$header = $this->encode([
				Headers::ALGORITHM => $this->getSigner()->getName(),
				Headers::TYPE => 'JWT',
			]);

			$now = $this->getCurrentTimeUTC();

			if (!isset($data[Claims::ISSUED_AT]))
				$data[Claims::ISSUED_AT] = $now;
			else if ($data[Claims::ISSUED_AT] instanceof \DateTime) {
				/** @var \DateTime $date */
				$date = clone $data[Claims::ISSUED_AT];

				$data[Claims::ISSUED_AT] = $date->setTimezone(new \DateTimeZone('UTC'))->getTimestamp();
			}

			if ($subject !== null)
				$data[Claims::SUBJECT] = $subject;

			if (!isset($data[Claims::EXPIRATION])) {
				if ($ttl === null)
					$ttl = $this->getTokenLifetime();

				$data[Claims::EXPIRATION] = $ttl > 0 ? $now + $ttl : 0;
			} else if ($data[Claims::EXPIRATION] instanceof \DateTime) {
				/** @var \DateTime $date */
				$date = clone $data[Claims::EXPIRATION];

				$data[Claims::EXPIRATION] = $date->setTimezone(new \DateTimeZone('UTC'))->getTimestamp();
			}

			if ($identifier = $this->getIdentifier())
				$data[Claims::ID] = $identifier->generate();

			$payload = $this->encode($data);

			$token = $header . '.' . $payload;

			return $token . '.' . $this->getSigner()->sign($this->getSecret(), $token);
		}

		/**
		 * @param string   $token
		 * @param array    $data
		 * @param int|null $ttl
		 *
		 * @return string
		 *
		 * @throws TokenExpiredException
		 * @throws TokenInvalidException
		 * @throws TokenNotYetValidException
		 */
		public function refresh($token, array $data = [], $ttl = null) {
			$payload = $this->parse($token);

			foreach ($payload->getClaims() as $key => $value) {
				if (in_array($key, [Claims::EXPIRATION, Claims::ISSUED_AT]))
					continue;

				$data[$key] = $value;
			}

			return $this->issue($data, null, $ttl);
		}

		/**
		 * @param string $token
		 *
		 * @return TokenInterface
		 *
		 * @throws TokenExpiredException
		 * @throws TokenInvalidException
		 * @throws TokenNotYetValidException
		 */
		public function parse($token) {
			$header = strtok($token, '.');
			$payload = strtok('.');
			$signature = strtok('');

			if (!$this->getSigner()->verify($this->getSecret(), $signature, $header . '.' . $payload))
				throw new TokenInvalidException();

			$payload = $this->decode($payload);

			$now = $this->getCurrentTimeUTC();

			if ($payload->exp > 0 && $now >= $payload->exp)
				throw new TokenExpiredException();
			else if (isset($payload->nbf) && $now <= $payload->nbf)
				throw new TokenNotYetValidException($payload->nbf);

			return new Token(new TokenHeaderSet((array)$this->decode($header)), (array)$payload);
		}

		/**
		 * @param array|object $data
		 *
		 * @return string
		 */
		protected function encode($data) {
			$data = json_encode($data);

			return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
		}

		/**
		 * @param string $data
		 *
		 * @return object
		 */
		protected function decode($data) {
			$data = base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));

			return json_decode($data);
		}

		/**
		 * @return int
		 */
		protected function getCurrentTimeUTC() {
			return (new \DateTime('now', new \DateTimeZone('UTC')))->getTimestamp();
		}
	}