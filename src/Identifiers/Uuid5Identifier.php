<?php
	namespace DaybreakStudios\Veritas\Identifiers;

	use Ramsey\Uuid\Uuid;

	class Uuid5Identifier extends AbstractUuidIdentifier {
		/**
		 * Uuid5Identifier constructor.
		 *
		 * @param string $namespace
		 * @param string $name
		 */
		public function __construct($namespace, $name) {
			parent::__construct(function() use ($namespace, $name) {
				return Uuid::uuid5($namespace, $name);
			});
		}
	}