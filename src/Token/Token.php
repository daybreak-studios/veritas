<?php
	namespace DaybreakStudios\Veritas\Token;

	use DaybreakStudios\Veritas\Claims;
	use DaybreakStudios\Veritas\Exception\EmptyFieldException;

	class Token implements TokenInterface {
		/**
		 * @var TokenHeaderSetInterface
		 */
		private $headers;

		/**
		 * @var array
		 */
		private $claims;

		/**
		 * Token constructor.
		 *
		 * @param TokenHeaderSetInterface $headers
		 * @param array                   $claims
		 */
		public function __construct(TokenHeaderSetInterface $headers, array $claims) {
			$this->headers = $headers;
			$this->claims = $claims;

			$this->transform(Claims::EXPIRATION, function($exp) {
				return \DateTime::createFromFormat('U', $exp > 0 ? $exp : PHP_INT_MAX);
			}, true);

			$this->transform(Claims::ISSUED_AT, function($iat) {
				return \DateTime::createFromFormat('U', $iat);
			}, true);

			$this->transform(Claims::NOT_BEFORE, function($nbf) {
				if ($nbf === 0)
					return null;

				return \DateTime::createFromFormat('U', $nbf);
			});
		}

		/**
		 * {@inheritdoc}
		 */
		public function getHeaders() {
			return $this->headers;
		}

		/**
		 * {@inheritdoc}
		 */
		public function getClaims() {
			return $this->claims;
		}

		/**
		 * {@inheritdoc}
		 */
		public function getId() {
			return @$this->claims[Claims::ID];
		}

		/**
		 * {@inheritdoc}
		 */
		public function getAudience() {
			return @$this->claims[Claims::AUDIENCE] ?: [];
		}

		/**
		 * {@inheritdoc}
		 */
		public function getExpirationDate() {
			return $this->claims[Claims::EXPIRATION];
		}

		/**
		 * {@inheritdoc}
		 */
		public function isExpired() {
			return $this->getExpirationDate()->getTimestamp() > time();
		}

		/**
		 * {@inheritdoc}
		 */
		public function getIssuedDate() {
			return $this->claims[Claims::ISSUED_AT];
		}

		/**
		 * {@inheritdoc}
		 */
		public function getIssuer() {
			return @$this->claims[Claims::ISSUER];
		}

		/**
		 * {@inheritdoc}
		 */
		public function getValidAfterDate() {
			return $this->claims[Claims::NOT_BEFORE];
		}

		/**
		 * {@inheritdoc}
		 */
		public function isValid() {
			$nbf = $this->getValidAfterDate();

			if ($nbf === null)
				return !$this->isExpired();

			return $nbf->getTimestamp() <= time() && !$this->isExpired();
		}

		/**
		 * {@inheritdoc}
		 */
		public function getSubject() {
			return @$this->claims[Claims::SUBJECT];
		}

		/**
		 * @param          $field
		 * @param callable $callback
		 * @param bool     $throwOnEmpty
		 *
		 * @throws EmptyFieldException
		 */
		protected function transform($field, callable $callback, $throwOnEmpty = false) {
			$value = @$this->claims[$field];

			if ($value === null && $throwOnEmpty)
				throw new EmptyFieldException($field);

			$this->claims[$field] = call_user_func($callback, $value);
		}
	}