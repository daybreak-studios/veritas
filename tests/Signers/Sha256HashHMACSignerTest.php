<?php
	namespace Signers;

	use DaybreakStudios\Veritas\Signers\Sha256HashHMACSigner;

	class Sha256HashHMACSignerTest extends AbstractHashHMACSignerTest {
		protected function createHashHMACSigner() {
			return new Sha256HashHMACSigner();
		}
	}