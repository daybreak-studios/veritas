<?php
	namespace DaybreakStudios\Veritas\TokenBuilder;

	interface TokenBuilderInterface {
		/**
		 * Sets the token's audience claim.
		 *
		 * An audience list a case-sensitive array of strings that identifies the parties that are meant to accept the
		 * token.
		 *
		 * @param string[] $audience
		 *
		 * @return $this
		 * @see https://tools.ietf.org/html/rfc7519#section-4.1.3
		 */
		public function audience(array $audience);

		/**
		 * Sets the token's expiration time claim.
		 *
		 * A token's expiration time claim identifies the timestamp after which the token should no longer be considered
		 * valid.
		 *
		 * @param \DateTime $expiresAt
		 *
		 * @return $this
		 * @see https://tools.ietf.org/html/rfc7519#section-4.1.4
		 */
		public function expiresAt(\DateTime $expiresAt);

		/**
		 * Sets the token's ID claim.
		 *
		 * A token's ID is a case-sensitive string that should have a negligible probability of being assigned to two or
		 * more tokens.
		 *
		 * @param string $id
		 *
		 * @return $this
		 * @see https://tools.ietf.org/html/rfc7519#section-4.1.7
		 */
		public function id($id);

		/**
		 * Sets the token's issued at claim.
		 *
		 * An "issued at" claim identifies the timestamp the token was issued, and is most commonly used to determine a
		 * token's age.
		 *
		 * @param \DateTime $issuedAt
		 *
		 * @return $this
		 * @see https://tools.ietf.org/html/rfc7519#section-4.1.6
		 */
		public function issuedAt(\DateTime $issuedAt);

		/**
		 * Sets the token's "issuer" claim.
		 *
		 * An "issuer" claim identifies the host or platform (referred to as the "principal") that issued the token.
		 *
		 * @param string $issuer
		 *
		 * @return $this
		 * @see https://tools.ietf.org/html/rfc7519#section-4.1.1
		 */
		public function issuer($issuer);

		/**
		 * Sets the token's subject.
		 *
		 * @param string $subject
		 *
		 * @return $this
		 */
		public function subject($subject);

		/**
		 * Sets the token's "not before" claim.
		 *
		 * A "not before" claim identifies the time at which the token becomes valid. Attempts to use the token before
		 * the time specified by this claim should be expected to result in failure.
		 *
		 * @param \DateTime $validAfter
		 *
		 * @return $this
		 * @see https://tools.ietf.org/html/rfc7519#section-4.1.5
		 */
		public function validAfter(\DateTime $validAfter);

		/**
		 * Builds and returns a new JWT.
		 *
		 * @return string
		 */
		public function issue();

		/**
		 * @return string[]|null
		 */
		public function getAudience();

		/**
		 * @return \DateTime|null
		 */
		public function getExpiresAt();

		/**
		 * @return string|null
		 */
		public function getId();

		/**
		 * @return \DateTime|null
		 */
		public function getIssuedAt();

		/**
		 * @return string|null
		 */
		public function getIssuer();

		/**
		 * @return string|null
		 */
		public function getSubject();

		/**
		 * @return \DateTime|null
		 */
		public function getValidAfter();
	}