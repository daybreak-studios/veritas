<?php
	namespace DaybreakStudios\Veritas\Signers;

	class Sha256HashHMACSigner extends HashHMACSigner {
		const NAME = 'HS256';
		const ALGO = 'sha256';

		public function __construct() {
			parent::__construct(self::NAME, self::ALGO);
		}
	}