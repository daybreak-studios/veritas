<?php
	namespace DaybreakStudios\Veritas\Identifiers;

	use Ramsey\Uuid\Uuid;

	class Uuid4Identifier extends AbstractUuidIdentifier {
		/**
		 * Uuid4Identifier constructor.
		 */
		public function __construct() {
			parent::__construct(function() {
				return Uuid::uuid4();
			});
		}
	}