<?php
	namespace DaybreakStudios\Veritas\Exception;
	
	class TokenExpiredException extends BadTokenException {
		public function __construct() {
			parent::__construct('The token provided has expired');
		}
	}