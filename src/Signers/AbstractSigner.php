<?php
	namespace DaybreakStudios\Veritas\Signers;

	abstract class AbstractSigner implements SignerInterface {
		private $name;

		public function __construct($name) {
			$this->name = $name;
		}

		public function getName() {
			return $this->name;
		}
	}