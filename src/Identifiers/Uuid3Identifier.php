<?php
	namespace DaybreakStudios\Veritas\Identifiers;

	use Ramsey\Uuid\Uuid;

	class Uuid3Identifier extends AbstractUuidIdentifier {
		/**
		 * Uuid3Identifier constructor.
		 *
		 * @param string $namespace
		 * @param string $name
		 */
		public function __construct($namespace, $name) {
			parent::__construct(function() use ($namespace, $name) {
				return Uuid::uuid3(UuidUtil::translateNamespace($namespace), $name);
			});
		}
	}