<?php
	namespace DaybreakStudios\Veritas\Identifiers;

	use Ramsey\Uuid\Uuid;

	final class UuidUtil {
		const NAMESPACE_MAP = [
			'DNS' => Uuid::NAMESPACE_DNS,
			'URL' => Uuid::NAMESPACE_URL,
			'OID' => Uuid::NAMESPACE_OID,
			'X500' => Uuid::NAMESPACE_X500,
		];

		/**
		 * Attempts to transform a namespace identifier into a namespace UUID.
		 *
		 * If the provided namespace is already a UUID, the UUID will simply be returned. If the namespace identifier
		 * cannot be mapped to a UUID, an exception will be thrown.
		 *
		 * @param string $namespace
		 *
		 * @return string
		 * @throws \Exception
		 */
		public static function translateNamespace($namespace) {
			if (strpos($namespace, '-') !== false)
				return $namespace;
			else if (array_key_exists($namespace, self::NAMESPACE_MAP))
				return self::NAMESPACE_MAP[$namespace];

			throw new \Exception($namespace . ' is not a recognized namespace identifier');
		}
	}