<?php
	namespace DaybreakStudios\Veritas\Signers;

	interface SignerInterface {
		/**
		 * @param string $key
		 * @param string $data
		 *
		 * @return string
		 */
		public function sign($key, $data);

		/**
		 * @param string $key
		 * @param string $signature
		 * @param string $data
		 *
		 * @return bool
		 */
		public function verify($key, $signature, $data);

		/**
		 * @return string
		 */
		public function getName();
	}