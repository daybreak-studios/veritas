<?php
	namespace DaybreakStudios\Veritas\Token;

	interface TokenInterface {
		/**
		 * @return TokenHeaderSetInterface
		 */
		public function getHeaders();

		/**
		 * @return array
		 */
		public function getClaims();

		/**
		 * @return mixed|null
		 */
		public function getId();

		/**
		 * @return array
		 */
		public function getAudience();

		/**
		 * @return \DateTime
		 */
		public function getExpirationDate();

		/**
		 * @return bool
		 */
		public function isExpired();

		/**
		 * @return \DateTime
		 */
		public function getIssuedDate();

		/**
		 * @return mixed|null
		 */
		public function getIssuer();

		/**
		 * @return \DateTime|null
		 */
		public function getValidAfterDate();

		/**
		 * Checks if the represented token has passed it's "not before" date if it's set), as well as if the token has
		 * not expired.
		 *
		 * @return bool
		 *
		 * @see TokenInterface::isExpired()
		 * @see TokenInterface::getValidAfterDate()
		 */
		public function isValid();

		/**
		 * @return mixed|null
		 */
		public function getSubject();
	}