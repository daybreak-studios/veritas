<?php
	namespace DaybreakStudios\Veritas\Identifiers;

	use Ramsey\Uuid\Uuid;

	class Uuid1Identifier extends AbstractUuidIdentifier {
		/**
		 * Uuid1Identifier constructor.
		 */
		public function __construct() {
			parent::__construct(function() {
				return Uuid::uuid1();
			});
		}
	}