<?php
	namespace DaybreakStudios\Veritas\Exception;

	class EmptyFieldException extends \Exception {
		/**
		 * EmptyFieldException constructor.
		 *
		 * @param string $field
		 */
		public function __construct($field) {
			parent::__construct(sprintf('The field %s is empty, and it should not be', $field));
		}
	}