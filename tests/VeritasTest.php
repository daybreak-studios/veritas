<?php
	use DaybreakStudios\Veritas\Claims;
	use DaybreakStudios\Veritas\Signers\Sha256HashHMACSigner;
	use DaybreakStudios\Veritas\Veritas;
	use PHPUnit\Framework\TestCase;

	class VeritasTest extends TestCase {
		const TTL = 60;
		const TEST_SUBJECT = 'foo@bar.baz';

		/**
		 * @var Veritas
		 */
		protected $veritas;

		protected function setUp() {
			$this->veritas = new Veritas(new Sha256HashHMACSigner(), uniqid('', true), self::TTL);
		}

		public function testEmptyIssue() {
			$token = $this->veritas->issue();
			$parsed = $this->veritas->parse($token);

			static::assertEquals(self::TTL, $parsed->getExpirationDate()->getTimestamp() - time(),
				'expiration time matches TTL', 1);
		}

		public function testIssueWithSubject() {
			$token = $this->veritas->issue([
				Claims::SUBJECT => self::TEST_SUBJECT,
			]);

			$parsed = $this->veritas->parse($token);

			static::assertEquals(self::TEST_SUBJECT, $parsed->getSubject(), 'parsed subject matches original subject');
			static::assertEquals(self::TTL, $parsed->getExpirationDate()->getTimestamp() - time(),
				'expiration time matches TTL', 1);
		}

		public function testIssueFromBuilder() {
			$token = $this->veritas->build()
				->subject(self::TEST_SUBJECT)
				->issuer($iss = uniqid())
				->issue();

			$parsed = $this->veritas->parse($token);

			static::assertEquals(self::TEST_SUBJECT, $parsed->getSubject(), 'parsed subject matches original subject');
			static::assertEquals(self::TTL, $parsed->getExpirationDate()->getTimestamp() - time(),
				'expiration time matches TTL', 1);
			static::assertEquals($iss, $parsed->getIssuer(), 'parsed issuer matches original issuer');
		}
	}