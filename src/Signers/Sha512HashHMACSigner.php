<?php
	namespace DaybreakStudios\Veritas\Signers;

	class Sha512HashHMACSigner extends HashHMACSigner {
		const NAME = 'HS512';
		const ALGO = 'sha512';

		public function __construct() {
			parent::__construct(self::NAME, self::ALGO);
		}
	}