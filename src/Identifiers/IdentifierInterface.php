<?php
	namespace DaybreakStudios\Veritas\Identifiers;

	interface IdentifierInterface {
		/**
		 * Generates a new unique identifier, meant for the 'jti' claim in a token.
		 *
		 * @return string
		 */
		public function generate();
	}