<?php
	namespace DaybreakStudios\Veritas\Signers;

	class HashHMACSigner extends AbstractSigner {
		private $algo;

		public function __construct($name, $algo) {
			parent::__construct($name);

			$this->algo = $algo;
		}

		public function getAlgorithm() {
			return $this->algo;
		}

		public function sign($key, $data) {
			return hash_hmac($this->getAlgorithm(), $data, $key);
		}

		public function verify($key, $signature, $data) {
			return $this->sign($key, $data) === $signature;
		}
	}