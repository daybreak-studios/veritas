<?php
	namespace DaybreakStudios\Veritas\Exception;

	class TokenNotYetValidException extends BadTokenException {
		public function __construct($nbf) {
			parent::__construct('The token provided is not valid before ' . $nbf);
		}
	}