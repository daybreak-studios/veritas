<?php
	namespace DaybreakStudios\Veritas\Token;

	use DaybreakStudios\Veritas\Exception\EmptyFieldException;

	interface TokenHeaderSetInterface {
		/**
		 * @return string
		 *
		 * @throws EmptyFieldException if the header field is null
		 */
		public function getAlgorithm();

		/**
		 * @return string
		 *
		 * @throws EmptyFieldException if the header field is null
		 */
		public function getType();
	}