<?php
	namespace Signers;

	use DaybreakStudios\Veritas\Signers\HashHMACSigner;
	use PHPUnit\Framework\TestCase;

	abstract class AbstractHashHMACSignerTest extends TestCase {
		/**
		 * @var HashHMACSigner
		 */
		protected $signer;

		/**
		 * @var string
		 */
		protected $data;

		/**
		 * @var string
		 */
		protected $secret;

		/**
		 * {@inheritdoc}
		 */
		protected function setUp() {
			$this->signer = $this->createHashHMACSigner();
			$this->data = rand(1000, 1000000);
			$this->secret = uniqid('', true);
		}

		/**
		 * @return HashHMACSigner
		 */
		protected abstract function createHashHMACSigner();

		public function testSign() {
			$hashed = hash_hmac($this->signer->getAlgorithm(), $this->data, $this->secret);
			$signed = $this->signer->sign($this->secret, $this->data);

			static::assertEquals($hashed, $signed, 'Signing yields the same result as manually running hash_hmac');
		}

		public function testVerify() {
			$signed = $this->signer->sign($this->secret, $this->data);

			static::assertTrue($this->signer->verify($this->secret, $signed, $this->data), 'signed data can be verified');
		}

		public function testVerifyHashHMAC() {
			$hashed = hash_hmac($this->signer->getAlgorithm(), $this->data, $this->secret);

			static::assertTrue($this->signer->verify($this->secret, $hashed, $this->data), 'hash_hmac can be verified');
		}
	}