<?php
	namespace DaybreakStudios\Veritas\TokenBuilder;

	use DaybreakStudios\Veritas\Claims;
	use DaybreakStudios\Veritas\Veritas;

	class TokenBuilder implements TokenBuilderInterface {
		/**
		 * @var Veritas
		 */
		protected $veritas;

		/**
		 * @var array
		 */
		protected $data = [];

		/**
		 * TokenBuilder constructor.
		 *
		 * @param Veritas $veritas
		 */
		public function __construct(Veritas $veritas) {
			$this->veritas = $veritas;
		}

		/**
		 * {@inheritdoc}
		 */
		public function audience(array $audience) {
			return $this->set(Claims::AUDIENCE, $audience);
		}

		/**
		 * {@inheritdoc}
		 */
		public function getAudience() {
			return $this->get(Claims::AUDIENCE);
		}

		/**
		 * {@inheritdoc}
		 */
		public function expiresAt(\DateTime $expiresAt) {
			return $this->set(Claims::EXPIRATION, $expiresAt);
		}

		/**
		 * {@inheritdoc}
		 */
		public function getExpiresAt() {
			return $this->get(Claims::EXPIRATION);
		}

		/**
		 * {@inheritdoc}
		 */
		public function id($id) {
			return $this->set(Claims::ID, $id);
		}

		/**
		 * {@inheritdoc}
		 */
		public function getId() {
			return $this->get(Claims::ID);
		}

		/**
		 * {@inheritdoc}
		 */
		public function issuedAt(\DateTime $issuedAt) {
			return $this->set(Claims::ISSUED_AT, $issuedAt);
		}

		/**
		 * {@inheritdoc}
		 */
		public function getIssuedAt() {
			return $this->get(Claims::ISSUED_AT);
		}

		/**
		 * {@inheritdoc}
		 */
		public function issuer($issuer) {
			return $this->set(Claims::ISSUER, $issuer);
		}

		/**
		 * {@inheritdoc}
		 */
		public function getIssuer() {
			return $this->get(Claims::ISSUER);
		}

		/**
		 * {@inheritdoc}
		 */
		public function subject($subject) {
			return $this->set(Claims::SUBJECT, $subject);
		}

		/**
		 * {@inheritdoc}
		 */
		public function getSubject() {
			return $this->get(Claims::SUBJECT);
		}

		/**
		 * {@inheritdoc}
		 */
		public function validAfter(\DateTime $validAfter) {
			return $this->set(Claims::NOT_BEFORE, $validAfter);
		}

		/**
		 * {@inheritdoc}
		 */
		public function getValidAfter() {
			return $this->get(Claims::NOT_BEFORE);
		}

		/**
		 * {@inheritdoc}
		 */
		public function issue() {
			$data = [];

			foreach ($this->data as $key => $value) {
				if ($value instanceof \DateTime)
					$value = $value->getTimestamp();

				$data[$key] = $value;
			}

			return $this->veritas->issue($data);
		}

		/**
		 * Sets a token claim.
		 *
		 * @param string $key
		 * @param mixed  $value
		 *
		 * @return $this
		 */
		protected function set($key, $value) {
			$this->data[$key] = $value;

			return $this;
		}

		/**
		 * Gets a token claim, or null if the claim is not set.
		 *
		 * @param string $key
		 *
		 * @return mixed|null
		 */
		protected function get($key) {
			if (!isset($this->data[$key]))
				return null;

			return $this->data[$key];
		}
	}