<?php
	namespace Signers;

	use DaybreakStudios\Veritas\Signers\Sha512HashHMACSigner;

	class Sha512HashHMACSignerTest extends AbstractHashHMACSignerTest {
		protected function createHashHMACSigner() {
			return new Sha512HashHMACSigner();
		}
	}