<?php
	namespace DaybreakStudios\Veritas;

	final class Claims {
		const AUDIENCE = 'aud';
		const EXPIRATION = 'exp';
		const ID = 'jti';
		const ISSUED_AT = 'iat';
		const ISSUER = 'iss';
		const NOT_BEFORE = 'nbf';
		const SUBJECT = 'sub';
	}