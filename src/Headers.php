<?php
	namespace DaybreakStudios\Veritas;

	final class Headers {
		const ALGORITHM = 'alg';
		const TYPE = 'typ';
	}