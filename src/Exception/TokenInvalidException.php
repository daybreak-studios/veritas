<?php
	namespace DaybreakStudios\Veritas\Exception;

	class TokenInvalidException extends BadTokenException {
		public function __construct() {
			parent::__construct('The token provided did not match it\'s signature');
		}
	}