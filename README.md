## Installation
```
$ composer require dbstudios/veritas
```

## Setup
To use Veritas, you'll need three things:

0. A [signer](#signers), which is used to sign payloads for tokens
0. A [secret key](#secrey-keys), which is used to sign and verify tokens
0. A [time-to-live value](#time-to-live) (in seconds) for your tokens


### Signers
There are several signers included with Veritas, and it's incredibly easy to
[create your own](#custom-signers). Veritas ships with the following signers, all of which should be
relatively self-explanatory.

- `DaybreakStudios\Veritas\Signers\HashHMACSigner`
- `DaybreakStudios\Veritas\Signers\Sha256HashHMACSigner`
- `DaybreakStudios\Veritas\Signers\Sha512HashHMACSigner`

The first, `HashHMACSigner`, uses PHP's [`hash_hmac`](http://php.net/manual/en/function.hash-hmac.php)
function to generate the signature. Which hashing function is used depends on how you call it's constructor.

```php
$signer = new HashHMACSigner('HS256', 'sha256');
```

The first argument is the name of the algorithm being used, and must correspond to the algorithm names
defined in the [RFC 7158](https://tools.ietf.org/html/rfc7518). The second argument is the name of the
hashing algorithm to use as first argument to [`hash_hmac`](http://php.net/manual/en/function.hash-hmac.php).

The Sha256 and Sha512 signers (the last two items on the list), both extend from `HashHMACSigner`, and
simply call it's constructor for you, meaning you can use them like so.

```php
$signer = new Sha256HashHMACSigner();
```

No constructor arguments are needed, as each class already knows the RFC name of the algorithm, as well as
which argument to pass to `hash_hmac`.

### Secret Keys
The secret key is a string that is used to sign your token. One **must** be provided. Be sure to store your
secrey key in a safe place, and to never commit it to git!

### Time To Live
The time to live (also shortened to TTL) is the amount of time, in seconds, that your token is valid for. A
token will always be rejected when calling `Veritas::parse()` if it's expiration date is in the past.

## Usage
There are three main methods of the `Veritas` class that you'll find yourself using: `issue`, `parse`, and `refresh`.

### Issuing A Token
You can create a new token by calling `issue`. The `issue` method takes up to three arguments: an array of additional
claims to include in the token, the subject of the token, and a time to live value. The example below uses all three,
but each one is optional.

```php
$veritas->issue([
    'my_custom_claim' => 'Hello, world!',
], 'tyler@lartonoix.com', 1600);
```

The example will yield a new token, with an additional claim named "my_custom_claim", a subject of "tyler@lartonoix.com",
and a TTL of 1600 seconds, or half an hour.

### Refreshing A Token
A token can be refreshed at any time before it expires. You can do this by calling the `refresh` method, like so.

```php
$token = $_SERVER['HTTP_AUTHORIZATION'];

$veritas->refresh($token);
```

Should you so choose, `refresh` also accepts to more optional arguments: an array of additional claims to add to the
new token, and a custom TTL value.

### Parseing A Token
The `parse` method allows you to read a token, and verify that it is valid.

```php
$token = $_SERVER['HTTP_AUTHORIZATION'];

try {
    $parsed = $veritas->parse($token);
} catch (BadTokenException $ex) {
    printf('Could not parse token: %s', $ex->getMessage());
}
```

The `parse` method will throw one of three exceptions in the event that something is wrong with the token.

|Exception|Reason|
|---------|------|
|TokenExpiredException|The token has passed it's expiration date|
|TokenInvalidException|The token could not be parsed, or the token's signature did not match the expected signature|
|TokenNotYetValidException|The token has not passed it's "not before" date|

All three exceptions extend from `BadTokenException`, so if you don't need to handle each case individually, you can
simply catch that one exception.

## Custom Signers
Please refer to [SignerInterface](src/Signers/SignerInterface.php) for which methods your signer will need to implement,
and [HashHMACSigner](src/Signers/HashHMACSigner.php) for an example implementation.