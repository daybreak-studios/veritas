<?php
	namespace DaybreakStudios\Veritas\Identifiers;

	class UniqidIdentifier implements IdentifierInterface {
		/**
		 * @var string
		 */
		private $prefix;

		/**
		 * @var bool
		 */
		private $entropy;

		/**
		 * UniqidIdentifier constructor.
		 *
		 * @param string $prefix
		 * @param bool   $entropy
		 */
		public function __construct($prefix = '', $entropy = true) {
			$this->prefix = $prefix;
			$this->entropy = $entropy;
		}

		/**
		 * {@inheritdoc}
		 */
		public function generate() {
			return uniqid($this->prefix, $this->entropy);
		}
	}