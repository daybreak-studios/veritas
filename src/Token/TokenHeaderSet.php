<?php
	namespace DaybreakStudios\Veritas\Token;

	use DaybreakStudios\Veritas\Exception\EmptyFieldException;
	use DaybreakStudios\Veritas\Headers;

	class TokenHeaderSet implements TokenHeaderSetInterface {
		/**
		 * @var array
		 */
		private $headers;

		/**
		 * TokenHeaderSet constructor.
		 *
		 * @param array $headers
		 */
		public function __construct(array $headers) {
			$this->headers = $headers;
		}

		/**
		 * {@inheritdoc}
		 */
		public function getAlgorithm() {
			return $this->getOrThrow(Headers::ALGORITHM);
		}

		/**
		 * {@inheritdoc}
		 */
		public function getType() {
			return $this->getOrThrow(Headers::TYPE);
		}

		/**
		 * @param string $field
		 *
		 * @return mixed
		 * @throws EmptyFieldException
		 */
		protected function getOrThrow($field) {
			if (!isset($this->headers[$field]))
				throw new EmptyFieldException($field);

			return $this->headers[$field];
		}
	}